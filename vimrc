
"
" TODO
" Home - moves to start of line
" wile shoud to start of current indent
"
" pathogen init
" call pathogen#infect()
filetype off
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()
" filetype on


" Supertab - this stuff dont work
" au FileType python set omnifunc=pythoncomplete#Complete
" let g:SuperTabDefaultCompletionType = "context"
" set completeopt=menuone,longest,preview


" disable pyflakes quickfix 
" http://vimdoc.sourceforge.net/htmldoc/quickfix.html#quickfix
let g:pyflakes_use_quickfix = 0
let NERDTreeQuitOnOpen=1
let NERDTreeShowHidden=1

" tabs movement
map <Esc>[1;3C :tabn<CR>
map <Esc>[1;3D :tabp<CR>
" imap <Esc>[1;3C <Esc>:tabn<CR>i
" imap <Esc>[1;3D <Esc>:tabp<CR>i

" Home & End
map <C-Home> :0<CR>
map <C-End> :$<CR>

map <Home> ^
imap <Home> <C-O>^

" ctrl + arrow move cursor between windows
map <C-Up> <C-w>k
map <C-Down> <C-w>j

map <C-Left> <C-w>h
map <C-Right> <C-w>l


" Nerd tree 
" autoload at start
" autocmd vimenter * NERDTree
" close vim if only tree left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" Nerd Tree
map <F3> :NERDTreeToggle<CR>
imap <F3> <Esc>:NERDTreeToggle<CR>

" Ultimate save
map <F2> :wa<CR>
imap <F2> <Esc>:wa<CR>a


" ALE
"map <F5> <Plug>(ale_previous_wrap)
" map <F6> <Plug>(ale_next_wrap)

map <F5> :ALEPrevious<cr>
imap <F5> <Esc>:ALEPrevious<cr>

map <F6> :ALENext<CR>
imap <F6> <Esc>:ALENext<CR>



" highligh & tabs
syntax on
filetype on
" this needed for pyflakes
filetype plugin on
" with this indent/python.vim will work
filetype plugin indent on
" au FileType python setlocal
set tabstop=4
set expandtab
set shiftwidth=4
set softtabstop=4
set autoindent
" to proper work on indent/python.vim
" set smartindent
" set smarttab

" blank space
set listchars=tab:--,trail:~
set list

set incsearch "Make search act like search in modern browsers


" show line numbers
set number

" open tag in a new tab
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>

" za - fold/unfold
" set foldmethod=indent
" set foldlevel=99


" No sound on errors
" set noerrorbells
" set novisualbell
" set t_vb=
" set tm=500


" highligh search toggle <F4>
noremap <F4> :set hlsearch! hlsearch?<CR>
imap <F4> <Esc>:set hlsearch! hlsearch?<CR>a

" initially highlight on
set hlsearch

" run python with :make
set makeprg=python\ %
" save before run
set autowrite

" trim trailing space
autocmd BufWritePre *.pl *.py, *.pyx *.c *.cpp :%s/\s\+$//e


" We are writing xhtml
let xml_use_xhtml = 1


" fix brackets colors
hi MatchParen cterm=none ctermbg=none ctermfg=white


" vim 7.3 and above
" set langmap=ёйцукенгшщзхъфывапролджэячсмитьбюЁЙЦУКЕHГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;`qwertyuiop[]asdfghjkl\\;'zxcvbnm\\,.~QWERTYUIOP{}ASDFGHJKL:\\"ZXCVBNM<>

" ru-en
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
map  <F1>  <Nop>
imap <F1>  <C-^>

" ultimate exit
map  <F10> :q<CR>
imap <F10> <Esc>:q<CR>

map <F12> :qa<CR>
imap <F12> <Esc>:qa<CR>


" Dont fucking move the cursor when i quit to normal!
" inoremap <Esc> <Esc>`^


set autoread        " перечитывать изменённые файлы автоматически


" перенос по словам, а не по буквам
set linebreak
set dy=lastline

"Подсвечиваем все что можно подсвечивать
let python_highlight_all = 1

" 256 цветов
set t_Co=256
" completition menu colors
highlight Pmenu ctermbg=238 gui=bold

" pyflakes colors
highlight SpellBad ctermbg=52 gui=bold




" PgUp PgDown mc style
map <silent> <PageUp> 1000<C-U>
map <silent> <PageDown> 1000<C-D>
imap <silent> <PageUp> <C-O>1000<C-U>
imap <silent> <PageDown> <C-O>1000<C-D>
" tells VIM during motion commands to try to preserve column where cursor is
" positioned.
set nostartofline



" jump to last cursor position when opening file
set viminfo='50,\"2000,:500,%,n~/.viminfo
autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal g`\"" | endif

" Ctrl-C/V/X copy/paste/cut into buffer t
vmap <C-X> "td
vmap <C-C> "ty`]
map  <C-V> "tP
" dont move to start after yank
vmap y y`]

" Ctrl-Space add blank line
noremap <C-@> :<C-U>call append(line(".") -1, repeat([''], v:count1))<CR>

" mc style line remove <C-y> <C-Del>
map <C-y>  dd
noremap <Esc>[3;5~ dd



let c_no_comment_fold=1
let c_no_if0_fold=1
let c_no_block_fold=1
" autocmd BufNewFile,BufRead *.c set foldenable foldmethod=syntax
" autocmd FileType *.[ch]{,pp}

" Win-Down/Up onpen close #ifdef folds.
map <Esc>[1;1A zc
map <Esc>[1;1B  zo
imap <Esc>[1;1A <Esc>zca
imap <Esc>[1;1B  <Esc>zo

" open/close all folds - win+PgUp/PgDown
map <Esc>[6;1~ zR
map <Esc>[5;1~ zM

" whitespace highlight
highlight SpecialKey ctermfg=238

" {} block navigation with C-[]
noremap <C-[> [{
noremap <C-]> ]}

" recursively search upward for tags file
set tags=./tags,tags;/


" cn - next error; zv - open fold; zz - cnte line on screen; :cc<Cr> - show
" error message
" F6 - F7 walk through tests errors
" map <F7> :cn<Cr>zvzz:cc<Cr>
" map <F6> :cp<Cr>zvzz:cc<Cr>

" Qfix toggle func
command -bang -nargs=? QFix call QFixToggle(<bang>0)
function! QFixToggle(forced)
  if exists("g:qfix_win") && a:forced == 0
    cclose
    unlet g:qfix_win
  else
    copen 10
    let g:qfix_win = bufnr("$")
  endif
endfunction
map <F8> :QFix<CR>

" hightlight color
highlight Search     ctermfg=black      ctermbg=71    cterm=NONE

" noswap
set noswapfile

colorscheme aqua
highlight SpellBad term=reverse ctermbg=1

" toggle comment
vmap <C-]> <leader>ci


